#include <cmath>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/spi.h>
#include <libopencm3/stm32/usart.h>
#include <string>
#include "usart.hpp"

class MAX31855
{
  /***************************************************
    This is a library for the Adafruit Thermocouple Sensor w/MAX31855K

    Designed specifically to work with the Adafruit Thermocouple Sensor
    ----> https://www.adafruit.com/products/269

    These displays use SPI to communicate, 3 pins are required to
    interface
    Adafruit invests time and resources providing this open source code,
    please support Adafruit and open-source hardware by purchasing
    products from Adafruit!

    Written by Limor Fried/Ladyada for Adafruit Industries.
    BSD license, all text above must be included in any redistribution
   ****************************************************/

public:
  MAX31855()
  {
  }

  float readInternal(void)
  {
    uint32_t v(spiRead32());

    // ignore bottom 4 bits - they're just thermocouple data
    v >>= 4;

    // pull the bottom 11 bits off
    float internal = v & 0x7FF;
    // check sign bit!
    if (v & 0x800)
    {
      // Convert to negative value by extending sign and casting to signed type.
      const int16_t tmp(0xF800 | (v & 0x7FF));
      internal = tmp;
    }
    internal *= 0.0625; // LSB = 0.0625 degrees
    //Serial.print("\tInternal Temp: "); Serial.println(internal);
    return internal;
  }

  bool readCelsius(float &t)
  {
    t = std::nan("1");
    int32_t v(spiRead32());
    if (v & 0x7)
    {
      return false;
    }

    if (v & 0x80000000)
    {
      // Negative value, drop the lower 18 bits and explicitly extend sign bits.
      v = 0xFFFFC000 | ((v >> 18) & 0x00003FFFF);
    }
    else
    {
      // Positive value, just drop the lower 18 bits.
      v >>= 18;
    }

    float centigrade(v);
    centigrade *= 0.25;

    if (centigrade < -800)
    {
      return false;
    }

    // LSB = 0.25 degrees C
    t = centigrade;
    return true;
  }

  uint8_t readError()
  {
    return spiRead32() & 0x7;
  }

private:
  uint32_t spiRead32(void)
  {
    uint32_t data(0);
    spi_enable(SPI1);
    data = spi_xfer(SPI1, 0);
    data <<= 8;
    data |= spi_xfer(SPI1, 0);
    data <<= 8;
    data |= spi_xfer(SPI1, 0);
    data <<= 8;
    data |= spi_xfer(SPI1, 0);
    spi_disable(SPI1);
    return data;
  }
};

void spi_init()
{
  rcc_periph_clock_enable(RCC_SPI1);
  gpio_set_mode(GPIOA,
                GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_ALTFN_PUSHPULL,
                GPIO4 | GPIO5 | GPIO7); // NSS = PA4, SCK = PA5, MOSI = PA7
  gpio_set_mode(GPIOA,
                GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT,
                GPIO6); // MISO = PA6
  rcc_periph_reset_pulse(RST_SPI1);
  spi_init_master(SPI1,
                  SPI_CR1_BAUDRATE_FPCLK_DIV_256,
                  SPI_CR1_CPOL_CLK_TO_0_WHEN_IDLE,
                  SPI_CR1_CPHA_CLK_TRANSITION_1,
                  SPI_CR1_DFF_8BIT,
                  SPI_CR1_MSBFIRST);
  spi_disable_software_slave_management(SPI1);
  spi_enable_ss_output(SPI1);
}

int main(void)
{
  rcc_clock_setup_pll(&rcc_hse_configs[RCC_CLOCK_HSE8_72MHZ]);
  rcc_periph_clock_enable(RCC_GPIOA);
  rcc_periph_clock_enable(RCC_GPIOC);
  gpio_set_mode(GPIOC,
                GPIO_MODE_OUTPUT_2_MHZ,
                GPIO_CNF_OUTPUT_PUSHPULL,
                GPIO13);
  gpio_set(GPIOC, GPIO13);
  usart_init();
  spi_init();
  MAX31855 th;

  while (1)
  {
    float temperature_celcius;
    th.readCelsius(temperature_celcius);

    send_usart("Temperature = ", true);
    if (std::isnan(temperature_celcius))
    {
      send_usart("NaN");
    }
    else
    {
      send_usart(std::to_string(temperature_celcius), true);
      send_usart(" °C");
    }

    for (std::size_t i(0); i < 2000000; ++i)
    {
      __asm__ volatile("nop");
    }
    gpio_toggle(GPIOC, GPIO13);
  }
  return 0;
}
