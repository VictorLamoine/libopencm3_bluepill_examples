#include <cstddef>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <string>
#include "usart.hpp"

int main(void)
{
  rcc_clock_setup_pll(&rcc_hse_configs[RCC_CLOCK_HSE8_72MHZ]);
  rcc_periph_clock_enable(RCC_GPIOC);
  gpio_set_mode(GPIOC,
                GPIO_MODE_OUTPUT_2_MHZ,
                GPIO_CNF_OUTPUT_PUSHPULL,
                GPIO13);
  usart_init();

  std::size_t count(0);
  while (1)
  {
    gpio_set(GPIOC, GPIO13);
    for (std::size_t i(0); i < 300000; ++i)
    {
      __asm__ volatile("nop");
    }

    gpio_clear(GPIOC, GPIO13);
    for (std::size_t i(0); i < 300000; ++i)
    {
      __asm__ volatile("nop");
    }

    send_usart("Count = " + std::to_string(count++));
  }

  return 0;
}
