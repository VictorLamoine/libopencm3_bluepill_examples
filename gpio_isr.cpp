#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/exti.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>

// Hook a button to PA0 / ground
// A press on the button will trigger the ISR and toggle the LED

// WARNING: No debounce on the LED / ISR!

void exti0_isr(void)
{
  exti_reset_request(EXTI0);
  gpio_toggle(GPIOC, GPIO13);
}

int main(void)
{
  rcc_clock_setup_pll(&rcc_hse_configs[RCC_CLOCK_HSE8_72MHZ]);
  rcc_periph_clock_enable(RCC_GPIOA);
  gpio_set_mode(GPIOA,
                GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_PULL_UPDOWN,
                GPIO0);
  gpio_clear(GPIOA, GPIO0); // Pull down resistor
  rcc_periph_clock_enable(RCC_GPIOC);
  gpio_set_mode(GPIOC,
                GPIO_MODE_OUTPUT_2_MHZ,
                GPIO_CNF_OUTPUT_PUSHPULL,
                GPIO13);
  gpio_set(GPIOC, GPIO13); // Turn off LED (polarity inversed)

  // Configure EXTI ISR
  rcc_periph_clock_enable(RCC_AFIO);
  exti_select_source(EXTI0, GPIOA);
  exti_set_trigger(EXTI0, EXTI_TRIGGER_RISING);
  exti_enable_request(EXTI0);
  nvic_enable_irq(NVIC_EXTI0_IRQ);

  while (1)
  {}
  return 0;
}
