#include <cstddef>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/scb.h>
#include <libopencm3/stm32/exti.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/pwr.h>
#include <libopencm3/stm32/rcc.h>

// Synopsis
// --------
// LED blinks a few times
// STM32 goes to stop mode (LED is off)
// Rising edge on PA0 wakes STM32 (press the button)
// LED stays on for a while
// Go to beginning (start again)

void exti0_isr(void)
{
  exti_reset_request(EXTI0);
}

int main(void)
{
  rcc_clock_setup_pll(&rcc_hse_configs[RCC_CLOCK_HSE8_72MHZ]);
  rcc_periph_clock_enable(RCC_GPIOC);
  gpio_set_mode(GPIOC,
                GPIO_MODE_OUTPUT_2_MHZ,
                GPIO_CNF_OUTPUT_PUSHPULL,
                GPIO13); // LED

  rcc_periph_clock_enable(RCC_GPIOA);
  gpio_set_mode(GPIOA,
                GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_PULL_UPDOWN,
                GPIO0); // Push button from 3V3 to PA0
  gpio_clear(GPIOA, GPIO0); // Pull down resistor

  // Configure wake up
  rcc_periph_clock_enable(RCC_AFIO); // EXTI
  exti_select_source(EXTI0, GPIOA); // PA0
  EXTI_IMR = 0x1; // Configure interrupt mask register for PA0
  exti_set_trigger(EXTI0, EXTI_TRIGGER_RISING);
  exti_enable_request(EXTI0);
  nvic_enable_irq(NVIC_EXTI0_IRQ); // PC0 interrupt

  while (1)
  {
    // Toggle LED
    for (std::size_t i(0); i < 30; ++i)
    {
      gpio_toggle(GPIOC, GPIO13);
      for (std::size_t j(0); j < 2000000; ++j)
      {
        __asm__ volatile("nop");
      }
    }
    gpio_set(GPIOC, GPIO13); // LED off

    // Configure sleep + go to sleep
    SCB_SCR |= SCB_SCR_SLEEPDEEP; // Set deepsleep bit
    pwr_set_stop_mode(); // Stop mode
    PWR_CR |= PWR_CR_LPDS; // Voltage regulator off
    exti_reset_request(EXTI0);
    __asm__ volatile("wfi"); // Sleep and wait for interrupt (rising edge) on PA0

    // HSI clock is used after stop mode
    rcc_clock_setup_pll(&rcc_hse_configs[RCC_CLOCK_HSE8_72MHZ]);
    SCB_SCR &= ~SCB_SCR_SLEEPDEEP; // Clear deepsleep bit

    // Keep LED on for some time
    gpio_clear(GPIOC, GPIO13); // LED on
    for (std::size_t i(0); i < 15; ++i)
    {
      for (std::size_t j(0); j < 2000000; ++j)
      {
        __asm__ volatile("nop");
      }
    }
  }

  return 0;
}
