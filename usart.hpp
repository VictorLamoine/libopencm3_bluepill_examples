#ifndef USART_HPP
#define USART_HPP

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/usart.h>
#include <string>

void usart_init(void)
{
  rcc_periph_clock_enable(RCC_GPIOA);
  rcc_periph_clock_enable(RCC_USART1);
  // UART TX is PA9
  gpio_set_mode(GPIOA,
                GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_ALTFN_PUSHPULL,
                GPIO_USART1_TX);

  usart_set_baudrate(USART1, 115200);
  usart_set_databits(USART1, 8);
  usart_set_stopbits(USART1, USART_STOPBITS_1);
  usart_set_mode(USART1, USART_MODE_TX);
  usart_set_parity(USART1, USART_PARITY_NONE);
  usart_set_flow_control(USART1, USART_FLOWCONTROL_NONE);
  usart_enable(USART1);
}

void send_usart(const std::string &s,
                const bool endline = true)
{
  for (std::size_t i(0); i < s.size(); ++i)
  {
    while (!usart_get_flag(USART1, USART_SR_TXE));
    usart_send(USART1, s.at(i));
  }

  if (endline)
  {
    while (!usart_get_flag(USART1, USART_SR_TXE));
    usart_send(USART1, '\r');
    while (!usart_get_flag(USART1, USART_SR_TXE));
    usart_send(USART1, '\n');
  }
}

#endif
