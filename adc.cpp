#include <cstddef>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/adc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include "usart.hpp"

uint16_t read_adc()
{
  adc_start_conversion_direct(ADC1);
  while (!adc_eoc(ADC1)) {}
  return adc_read_regular(ADC1);
}

int main(void)
{
  rcc_clock_setup_pll(&rcc_hse_configs[RCC_CLOCK_HSE8_72MHZ]);
  rcc_periph_clock_enable(RCC_GPIOC);
  gpio_set_mode(GPIOC,
                GPIO_MODE_OUTPUT_2_MHZ,
                GPIO_CNF_OUTPUT_PUSHPULL,
                GPIO13);
  gpio_set(GPIOC, GPIO13); // LED off

  rcc_periph_clock_enable(RCC_GPIOA);
  gpio_set_mode(GPIOA,
                GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_ANALOG,
                GPIO0); // PA0
  usart_init();

  // Configure ADC
  const uint8_t adc_channel(ADC_CHANNEL0); // PA0
  rcc_peripheral_enable_clock(&RCC_APB2ENR, RCC_APB2ENR_ADC1EN);
  adc_power_off(ADC1);
  rcc_peripheral_reset(&RCC_APB2RSTR, RCC_APB2RSTR_ADC1RST);
  rcc_peripheral_clear_reset(&RCC_APB2RSTR, RCC_APB2RSTR_ADC1RST);
  adc_set_dual_mode(ADC_CR1_DUALMOD_IND);
  rcc_set_adcpre(RCC_CFGR_ADCPRE_PCLK2_DIV6);
  adc_disable_scan_mode(ADC1);
  adc_set_right_aligned(ADC1);
  adc_set_single_conversion_mode(ADC1);
  uint8_t channel_array[] = { adc_channel };
  adc_set_regular_sequence(ADC1, 1, channel_array);
  adc_set_sample_time(ADC1, adc_channel, ADC_SMPR_SMP_239DOT5CYC);
  adc_power_on(ADC1);
  adc_reset_calibration(ADC1);
  adc_calibrate_async(ADC1);
  while (adc_is_calibrating(ADC1));

  while (1)
  {
    gpio_toggle(GPIOC, GPIO13);
    uint32_t adc(read_adc());
    send_usart(std::to_string(adc));

    for (std::size_t i(0); i < 500000; ++i)
    {
      __asm__ volatile("nop");
    }
  }

  return 0;
}
